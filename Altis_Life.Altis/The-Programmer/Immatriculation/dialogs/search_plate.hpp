/*
    Author: Jean-Baptiste
    Teamspeak 3: ts.the-programmer.com
    Web site: www.the-programmer.com

    Terms of use:
        - This file is forbidden unless you have permission from the author. If you have this file without permission to use it please do not use it and do not share it.
        - If you have permission to use this file, you can use it on your server however it is strictly forbidden to share it.
        - Out of respect for the author please do not delete this information.
*/
class max_search_plate {
   idd = 8990;
   name = "max_search_plate";
   movingenable = 0;
   enablesimulation = 1;
   onload = "[] spawn max_immat_fnc_plateSearchMenu;";
   class controlsBackground
   {
       class fond : Life_RscPicture
       {
           idc = 1002;
           text = "";
           x = 0.249479166666666 * safezoneW + safezoneX;
           y = 0.0724926253687316 * safezoneH + safezoneY;
           w = 0.5 * safezoneW;
           h = 0.85 * safezoneH;
       };
   };
   class controls
   {
       class Baresearch : Life_RscEditWithNoBorderPlate
       {
           idc = 1003;
           text = "";
           x = 0.3390625 * safezoneW + safezoneX;
           y = 0.506391347099312 * safezoneH + safezoneY;
           w = 0.21875 * safezoneW;
           h = 0.0345585054080633 * safezoneH;
           colortext[] = {1,1,1,1};
       };
       class Search : Life_RscButtonMenuPlateInvisible
       {
           idc = -1;
           text = "";
           onbuttonclick = "[] spawn max_immat_fnc_infoPlate;";
           x = 0.559895833333334 * safezoneW + safezoneX;
           y = 0.506391347099312 * safezoneH + safezoneY;
           w = 0.1 * safezoneW;
           h = 0.0345585054080633 * safezoneH;
       };
       class warning : Life_RscPicture
       {
           idc = 1004;
           text = "";
           x = 0.3390625 * safezoneW + safezoneX;
           y = 0.549385447394298 * safezoneH + safezoneY;
           w = 0.03 * safezoneW;
           h = 0.05 * safezoneH;
       };
       class no_plate : Life_RscStructuredText
       {
           idc = 1005;
           x = 0.369270833333333 * safezoneW + safezoneX;
           y = 0.554771386430678 * safezoneH + safezoneY;
           h = 0.04 * safezoneH;
           w = 0.25 * safezoneW;
       };
   };
};