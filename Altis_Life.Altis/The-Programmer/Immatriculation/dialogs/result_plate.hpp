/*
    Author: Jean-Baptiste
    Teamspeak 3: ts.the-programmer.com
    Web site: www.the-programmer.com

    Terms of use:
        - This file is forbidden unless you have permission from the author. If you have this file without permission to use it please do not use it and do not share it.
        - If you have permission to use this file, you can use it on your server however it is strictly forbidden to share it.
        - Out of respect for the author please do not delete this information.
*/
class max_result_plate {
   idd = 6750;
   name = "max_result_plate";
   movingenable = 0;
   enablesimulation = 1;
   class controlsBackground
   {
       class fond : Life_RscPicture
       {
           idc = 1002;
           text = "";
           x = 0.240104166666666 * safezoneW + safezoneX;
           y = 0.08625860373648 * safezoneH + safezoneY;
           w = 0.5 * safezoneW;
           h = 0.85 * safezoneH;
       };
   };
   class controls
   {
       class owner : Life_RscText
       {
           idc = 1003;
           x = 0.428749999999999 * safezoneW + safezoneX;
           y = 0.447593411996067 * safezoneH + safezoneY;
           h = 0.027 * safezoneH;
           w = 0.17 * safezoneW;
           text = "";
           size = "(((((safezonew / safezoneh) min 1.2) / 1.2) / 25) * 1)";
       };
       class model : Life_RscText
       {
           idc = 1004;
           x = 0.428749999999999 * safezoneW + safezoneX;
           y = 0.50319075712881 * safezoneH + safezoneY;
           h = 0.028 * safezoneH;
           w = 0.17 * safezoneW;
           text = "";
           size = "(((((safezonew / safezoneh) min 1.2) / 1.2) / 25) * 1)";
       };
       class color : Life_RscText
       {
           idc = 1005;
           x = 0.428749999999999 * safezoneW + safezoneX;
           y = 0.531705998033432 * safezoneH + safezoneY;
           h = 0.028 * safezoneH;
           w = 0.17 * safezoneW;
           text = "";
           size = "(((((safezonew / safezoneh) min 1.2) / 1.2) / 25) * 1)";
       };
       class side : Life_RscText
       {
           idc = 1006;
           x = 0.428749999999999 * safezoneW + safezoneX;
           y = 0.560221238938053 * safezoneH + safezoneY;
           h = 0.028 * safezoneH;
           w = 0.17 * safezoneW;
           text = "";
           size = "(((((safezonew / safezoneh) min 1.2) / 1.2) / 25) * 1)";
       };
       class type : Life_RscText
       {
           idc = 1007;
           x = 0.428749999999999 * safezoneW + safezoneX;
           y = 0.588736479842674 * safezoneH + safezoneY;
           h = 0.028 * safezoneH;
           w = 0.17 * safezoneW;
           text = "";
           size = "(((((safezonew / safezoneh) min 1.2) / 1.2) / 25) * 1)";
       };
       class registration : Life_RscText
       {
           idc = 1001;
           x = 0.428749999999999 * safezoneW + safezoneX;
           y = 0.475125368731637 * safezoneH + safezoneY;
           h = 0.0279999999999999 * safezoneH;
           w = 0.17 * safezoneW;
           text = "";
           size = "(((((safezonew / safezoneh) min 1.2) / 1.2) / 25) * 1)";
       };
   };
};