/*
    Author: Jean-Baptiste
    Teamspeak 3: ts.the-programmer.com
    Web site: www.the-programmer.com

    Terms of use:
        - This file is forbidden unless you have permission from the author. If you have this file without permission to use it please do not use it and do not share it.
        - If you have permission to use this file, you can use it on your server however it is strictly forbidden to share it.
        - Out of respect for the author please do not delete this information.
*/
class custom_plate {
   idd = 9000;
   name = "custom_plate";
   movingenable = 0;
   enablesimulation = 1;
   class controlsBackground
   {
       class fond : Life_RscPicture
       {
           idc = 1002;
           text = "";
           x = 0.247916666666666 * safezoneW + safezoneX;
           y = 0.094124877089479 * safezoneH + safezoneY;
           w = 0.5 * safezoneW;
           h = 0.85 * safezoneH;
       };
   };
   class controls
   {
       class Baresearch : Life_RscEditWithNoBorderPlate
       {
           idc = 8991;
           text = "";
           x = 0.377395833333334 * safezoneW + safezoneX;
           y = 0.522261553588987 * safezoneH + safezoneY;
           w = 0.24 * safezoneW;
           h = 0.035 * safezoneH;
           colortext[] = {1,1,1,1};
       };
       class Ok : Life_RscButtonMenuPlateInvisible
       {
           idc = 5442;
           text = "";
           onbuttonclick = "[] spawn max_immat_fnc_customPlate;";
           x = 0.431708333333333 * safezoneW + safezoneX;
           y = 0.631409046214356 * safezoneH + safezoneY;
           w = 0.13 * safezoneW;
           h = 0.035 * safezoneH;
       };
       class list : Life_RscCombo
       {
           x = 0.415104166666667 * safezoneW + safezoneX;
           y = 0.459376106194691 * safezoneH + safezoneY;
           w = 0.164583333333333 * safezoneW;
           h = 0.03 * safezoneH;
           idc = 1001;
       };
   };
};