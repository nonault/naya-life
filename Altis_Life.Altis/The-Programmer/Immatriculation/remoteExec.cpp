class max_immat_fnc_getPlateInfo {
    allowedTargets = 2;
};
class max_immat_fnc_updatePlate {
    allowedTargets = 2;
};
class max_immat_fnc_initVehicleImmatriculation {
    allowedTargets = 2;
};
class max_immat_fnc_isPlateExist {
	allowedTargets = 2;
};
class max_immat_fnc_customPlateConfirm {
	allowedTargets = 1;
};