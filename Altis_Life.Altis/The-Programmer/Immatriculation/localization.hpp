class Immatriculation_Localization {
	class STR_IN_VEHICLE {
		en = "You must get out of your vehicle....";
		fr = "Vous devez sortir de votre véhicule...";
		de = "Sie müssen aus Ihrem Fahrzeug aussteigen...";
		es = "Tienes que salir de tu vehículo.";
	};
	class STR_DEFAULT_TEXT_IN_SEARCH_BAR {
		en = "License plate";
		fr = "Immatriculation";
		de = "Nummernschild";
		es = "Matricula";
	};
	class STR_UNKNOW_PLATE {
		en = "Plate unreadable, it seems tampered !";
		fr = "Plaque illisible, elle semble trafiquée !";
		de = "Nummernschild unlesbar, es scheint Manipuliert zu sein !";
		es = "Matricula ilegible, parece manipulada";
	};
	class STR_RESULT_PLATE {
		en = "<t color='#FF0000' t size='2'>License Plate</t><br/><t size='1.25'>%1";
		fr = "<t color='#FF0000' t size='2'>Immatriculation</t><br/><t size='1.25'>%1";
		de = "<t color='#FF0000' t size='2'>Nummernschild</t><br/><t size='1.25'>%1";
		es = "<t color='#FF0000' t size='2'>Matricula</t><br/><t size='1.25'>%1";
	};
	class STR_FAIL_PLATE_ENTER {
		en = "Invalid license plate !";
		fr = "Plaque d'immatriculation invalide !";
		de = "Ungültiges Nummernschild !";
		es = "Matricula invalida !";
	};
	class STR_NO_VEHICLES_FOUND {
		en = "No vehicle found !";
		fr = "Aucun véhicule trouvé !";
		de = "Kein Fahrzeug gefunden !";
		es = "No se ha encontrado nigun vehiculo !";
	};
	class STR_NOT_ENOUGHT_MONEY {
		en = "You don't have enough money in your wallet ! It costs you %1$.";
		fr = "Vous n'avez pas asser d'argent sur vous ! Cela vous coûte %1€.";
		de = "Du hast nicht genug geld ! Es kostet %1€.";
		es = "No tienes suficiente dinero en la cartera ! Te cuesta %1€.";
	};
	class STR_VEHICLE_BREAK {
		en = "It seems that your vehicle is totally broken ! You must have it repaired...";
		fr = "Il semblerais que votre véhicule soit totalement cassé ! Vous devez le faire réparer...";
		de = "Scheint so als hätte dein Fahrzeug einen Totalschaden ! Du musst es reparieren lassen...";
		es = "Parece que este vehiculo esta completamente roto ! Tienes que repararlo...";
	};
	class STR_YOU_PAID {
		en = "You paid %1$.";
		fr = "Vous avez payé %1€.";
		de = "Du hast %1€ bezahlt.";
		es = "Has pagado %1€.";
	};
	class STR_CHANGING_PLATE {
		en = "Changing your license plate in progress !";
		fr = "Changement de la plaque d'immatriculation en cours !";
		de = "Änderung deines Nummernschildes in Gange !";
		es = "Cambio de matricula en progreso !";
	};
	class STR_FAKING_PLATE {
		en = "Hiding the license plate in progress !";
		fr = "Masquage de la plaque d'immatriculation en cours !";
		de = "Nummernschild wird unerkennbar gemacht !";
		es = "Ocultando la matricula en progreso !";
	};
	class STR_FINISH_FAKE_PLATE {
		en = "Your vehicle is ready, be careful your plate is now hidden, this is forbidden by the police because they can't find your name !";
		fr = "Votre véhicule est prêt, attention votre plaque est désormais illisible, ceci est interdit par la police car elle ne peux pas trouver votre nom !";
		de = "Dein Fahrzeug steht bereit, sei vorsichtig dein Nummernschild ist nun nicht Sichtbar, laut der Polizei unerlaubt weil sie deinen Namen nicht finden kann !";
		es = "Tu vehiculo esta listo, ten cuidado, tu matricula esta ahora oculta, es ilegal porque la policia no puede encontrar tu nombre !";
	};
	class STR_FINISH_NEW_PLATE {
		en = "Your vehicle is ready with a new plate !";
		fr = "Votre véhicule est prêt avec une nouvelle plaque !";
		de = "Dein Fahrzeug ist mit einem Neuen Nummernschild bereit !";
		es = "Tu vehiculo esta listo con una matricula nueva !";
	};
	class STR_ADDACTION_CHANGE_PLATE {
		en = "Change license plate";
		fr = "Changer de plaque d'immatriculation";
		de = "Nummernschild ändern";
		es = "Cambiar matricula";
	};
	class STR_ADDACTION_MASK_PLATE {
		en = "Hide the license plate";
		fr = "Masquer la plaque d'immatriculation";
		de = "Nummernschild verdecken";
		es = "Ocultar matricula";
	};
	class STR_ADDACTION_SEARCH_PLATE {
		en = "File Registrations";
		fr = "Fichier Immatriculations";
		de = "Datei Registrierung";
		es = "Archivos de matriculacion";
	};
    class STR_TYPE_CAR_LAND {
        en = "Car";
        fr = "Terrestre";
		de = "Auto";
		es = "Coche";
    };
    class STR_TYPE_CAR_AIR {
        en = "Air";
        fr = "Aérien";
		de = "Luft";
		es = "Aereo";
    };
    class STR_TYPE_CAR_BOAT {
        en = "Ship";
        fr = "Maritime";
		de = "Boot";
		es = "Barcos";
    };
    class STR_TYPE_COP {
        en = "Police";
        fr = "Gendarmerie";
		de = "Polizei";
		es = "Policia";
    };
    class STR_TYPE_CIV {
        en = "Civil";
        fr = "Civil";
		de = "Zivilist";
		es = "Civil";
    };
    class STR_TYPE_MED {
        en = "Medical";
        fr = "Médical";
		de = "Medic";
		es = "Medico";
    };
    class STR_COLOR_DEFAULT {
        en = "Default";
        fr = "Défaut";
		de = "Standart";
		es = "Por defecto";
    };
	class STR_RENTAL_VEHICLE {
		en = "Rental car : Rented by %1";
		fr = "Vehicule de location : Loué par %1";
		de = "Mietwagen : Vermietet von %1";
		es = "Alquiler de vehiculos : Alquilado por %1";
	};
	class STR_RENTED_VEHICLE {
		en = "This vehicle is a rental car, you can not change its license plate.";
		fr = "Ce véhicule est un véhicule de location, vous ne pouvez pas modifier sa plaque.";
		de = "Dieses Fahrzeug ist ein Mietauto, Sie können sein Kennzeichen nicht ändern.";
		es = "Este vehículo es un vehículo de alquiler, no puede cambiar su matrícula.";
	};
    class STR_CONFIRM_CHANGE_PLATE_HEADER {
        en = "Plate change";
        fr = "Changement de plaque";
		de = "Plattenwechsel";
		es = "Cambio de placa";
    };
    class STR_CONFIRM_CHANGE_PLATE_TEXT {
        en = "The plate change costs %1$. Do you want to continue ?";
        fr = "Le changement de plaque coute %1€. Voulez vous continuer ?";
		de = "Der Plattenwechsel kostet %1€. Möchtest du fortfahren ?";
		es = "El cambio de placa cuesta %1€. ¿ Quieres continuar ?";
    };
    class STR_CONFIRM_MASK_PLATE_HEADER {
        en = "Hide the plate";
        fr = "Masquer la plaque";
		de = "Das Nummernschild ausblenden";
		es = "Ocultar la placa";
    };
    class STR_CONFIRM_MASK_PLATE_TEXT {
        en = "Hide the plate costs %1$. Do you want to continue ?";
        fr = "Masquer la plaque coute %1€. Voulez vous continuer ?";
		de = "Verstecken Sie die Platte kostet %1€. Möchtest du fortfahren ?";
		es = "Ocultar los costos de la placa %1€. ¿ Quieres continuar ?";
    };
    class STR_YES {
        en = "Confirm";
        fr = "Confirmer";
        de = "Bestätigen";
		es = "Confirmar";
    };
    class STR_NO {
        en = "Cancel";
        fr = "Annuler";
        de = "Abbrechen";
		es = "Cancelar";
    };
    class STR_ALREADY_MASKED {
        en = "The vehicle plate is already hidden";
        fr = "La plaque du véhicule est déjà masquée";
        de = "Das Fahrzeugkennzeichen ist bereits verborgen";
		es = "La placa del vehículo ya está oscurecida";
    };
    class STR_ADDACTION_CUSTOM_PLATE {
        en = "Customize the plate";
        fr = "Personnaliser la plaque";
        de = "Personalisierung des Nummernschildes";
		es = "Personalizar la matrícula";
    };
    class STR_CHECK_CUSTOM_PLATE {
        en = "Checking the availability of the license plate...";
        fr = "Vérification de la disponibilité de la plaque d'immatriculation...";
        de = "Überprüfung der Verfügbarkeit des Nummernschildes...";
		es = "Comprobación de la disponibilidad de la matrícula...";
    };
    class STR_MIN_CHARACTERS_PLATE {
        en = "The license plate must have at least %1 characters !";
        fr = "La plaque d'immatriculation doit avoir au minimum %1 caractères !";
        de = "Das Nummernschild muss mindestens %1 Zeichen lang sein!";
		es = "La placa de matrícula debe tener al menos %1 de caracteres!";
    };
    class STR_MAX_CHARACTERS_PLATE {
        en = "The license plate cannot exceed %1 characters !";
        fr = "La plaque d'immatriculation ne peux pas dépasser %1 caractères !";
        de = "Das Nummernschild darf %1 Zeichen nicht überschreiten !";
		es = "La matrícula no puede tener más de %1 de caracteres !";
    };
    class STR_PLATE_NOT_AVAILABLE {
        en = "This license plate already exists !";
        fr = "Cette plaque d'immatriculation existe déjà !";
        de = "Dieses Nummernschild existiert bereits !";
		es = "Esta matrícula ya existe !";
    };
    class STR_PLATE_NOT_ALLOWED {
        en = "This license plate is not allowed !";
        fr = "Cette plaque d'immatriculation n'est pas autorisée !";
        de = "Dieses Nummernschild ist nicht erlaubt !";
		es = "Esta matrícula no está permitida !";
    };
    class STR_PLATE_EMPTY {
        en = "Please enter the license plate of your choice.";
        fr = "Veuillez entrer la plaque d'immatriculation de votre choix.";
        de = "Bitte geben Sie das Nummernschild Ihrer Wahl ein.";
		es = "Por favor, introduzca la matrícula de su elección.";
    };
    class STR_STAY_NEAR_CAR {
        en = "You must remain close to and outside the vehicle during the modification of the license plate, action cancelled.";
        fr = "Vous devez rester proche et à l'extérieur du véhicule pendant la modification de la plaque d'immatriculation, action annulée.";
        de = "Sie müssen während der Änderung des Kennzeichens in der Nähe und außerhalb des Fahrzeugs bleiben, Aktion abgebrochen.";
		es = "Usted debe permanecer cerca y fuera del vehículo durante la modificación de la matrícula, acción cancelada.";
    };
    class STR_BAD_CHARACTER {
        en = "You use unauthorized characters, please use only characters or numbers from the alphabet !";
        fr = "Vous utilisez des caractères non autorisés, veuillez utiliser uniquement des caractères ou des chiffres de l'alphabet !";
        de = "Sie verwenden nicht autorisierte Zeichen, bitte verwenden Sie nur Zeichen oder Zahlen aus dem Alphabet !";
		es = "Usted utiliza caracteres no autorizados, por favor utilice sólo caracteres o números del alfabeto !";
    };
    class STR_MUST_RESPECT_FORMAT_PLATE {
        en = "The plate is not in the right format ! You must respect this type of plate : %1";
        fr = "La plaque n'est pas dans le bon format ! Vous devez respecter ce type de plaque : %1";
        de = "Die Platte hat nicht das richtige Format ! Sie müssen diesen Plattentyp respektieren : %1";
		es = "¡ La placa no está en el formato correcto ! Debes respetar este tipo de placa : %1";
    };
    class STR_RENTAL_PLATE {
        en = "RENTAL";
        fr = "LOCATION";
        de = "VERMIETUNG";
		es = "ALQUILER";
    };
};