/*
    Author: Maxence
    Web site: www.the-programmer.com
    Discord: https://discord.the-programmer.com

    Terms of use:
        - This file is forbidden unless you have permission from the author. If you have this file without permission to use it please do not use it and do not share it.
        - If you have permission to use this file, you can use it on your server however it is strictly forbidden to share it.
        - Out of respect for the author please do not delete this information.
*/
#define false 0
#define true 1

class Max_Settings_Immatriculation {
    default_lang = "fr"; // fr / en / es / de

    textures_base_path = "The-Programmer\Immatriculation\textures";
    tonic_version = 5; //5 if you are on version 5.0 or 4 if you are on version 4.X

    price_new_plate = 15000; // PRICE TO CHANGE LICENSE PLATE
    price_mask_plate = 100000; // PRICE TO HIDE LICENSE PLATE
    price_plateCustom = 50000; // Price for a personalized plate

    minCharacters = 3; // Minimum number of characters allowed for the personalized plate
    maxCharacters = 15; // Maximum number of characters allowed for the personalized plate, /!\ Max 15 characters can be displayed on the vehicle
    forbiddenWord[] = {"hitler","dick","acab"};

    carPlateFormat = "LL-DDD-LL"; // Format of the plate (default French format), L = random letter, D = random digit (0 -> 9). For example, a format "LLDD LLL" (UK) can give as result : "AB12 CDE"
    shipPlateFormat = "LLLLL";
    airPlateFormat = "F-LLLL";
    mustRespectFormatForCustomPlate = false; // On true, even for custom plate, players will have to respect the plateFormat

    timeNewPlate = 30; // In seconds
    timeMaskPlate = 30; // In seconds
    timePlateCustom = 30; // In seconds
};
