/*
_null=this execVM "The-Programmer\Immatriculation\init.sqf";
*/
_this allowDamage false; 
_this enableSimulation false;
_this addAction[(["STR_ADDACTION_CHANGE_PLATE","Max_Settings_Immatriculation","Immatriculation_Localization"] call theprogrammer_core_fnc_localize),{[] spawn max_immat_fnc_newPlate;}];
_this addAction[(["STR_ADDACTION_CUSTOM_PLATE","Max_Settings_Immatriculation","Immatriculation_Localization"] call theprogrammer_core_fnc_localize),{[] spawn max_immat_fnc_customPlateMenu}];
_this addAction[(["STR_ADDACTION_MASK_PLATE","Max_Settings_Immatriculation","Immatriculation_Localization"] call theprogrammer_core_fnc_localize),{[] spawn max_immat_fnc_maskPlate;}];