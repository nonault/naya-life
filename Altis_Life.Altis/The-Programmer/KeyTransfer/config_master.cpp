/*
    Author: Maxence
    Web site: www.the-programmer.com
    Discord : https://discord.the-programmer.com

    Terms of use:
        - This file is forbidden unless you have permission from the author. If you have this file without permission to use it please do not use it and do not share it.
        - If you have permission to use this file, you can use it on your server however it is strictly forbidden to share it.
        - Out of respect for the author please do not delete this information.
*/
#define true 1
#define false 0

class Max_Settings_GiveVehicle {
    default_lang = "fr"; // fr / en / de / es

    tonic_version = 5; // 5 if you are on version 5.0 or 4 if you are on version 4.X

    maximum_give_price_ratio = 0.75; // The price to give away the vehicle or house cannot exceed this percentage of the original purchase price
    maximum_key_price_ratio = 0.50; // The price for lending the keys to the vehicle or house cannot exceed this percentage of the original purchase price

    can_give_object_permanently = true; // Allow players to change the ownership of their vehicles or houses
    can_give_vehicles = true;
    can_give_houses = true;

    can_give_keys_temporary = true; // Allow players to give a copy of the keys to their vehicle or house for a set period of time
    can_give_keys_permanently = true; // Allow players to give away a copy of the keys to their car or house for an infinite amount of time

    minimum_duration = -1; // Minimum duration in days for which a player can give the keys of his vehicle, -1 = no minimum
    maximum_duration = -1; // Maximum duration in days for which a player can give the keys of his vehicle, -1 = no limit

    can_unimpound_vehicle_temporary_keys = true; // If the owner of the vehicle is offline, the player who has the keys may or may not take the vehicle out himself in a special garage
};
