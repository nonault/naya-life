class KeyTransfer_Localization {
	class STR_SUCCES_GIVE_OBJECT {
		en = "You have given your %1 PERMANENTLY to %2.";
		fr = "Vous avez donné votre %1 de manière PERMANANTE à %2.";
		de = "Du hast deinen %1 PERMANENT an %2 gegeben.";
		es = "Le has dado PERMANENTEMENTE tu %1 a %2.";
	};
	class STR_SUCCES_RECEIVE_OBJECT {
		en = "%1 has transferred ownership of its %2 to you.";
		fr = "%1 vous a transféré la propriété de son/sa %2.";
		de = "%1 hat das Eigentum an seinem %2 an Sie übertragen.";
		es = "%1 le ha transferido la propiedad de su %2.";
	};
	class STR_SUCCES_RECEIVE_KEY_TEMP {
		en = "%1 gave you a copy of the keys to its %2 for %3 days.";
		fr = "%1 vous a donné un double des clés de son/sa %2 pendant %3 jours.";
		de = "%1 gab Ihnen eine Kopie der Schlüssel zu seinem %2 für %3 Tage.";
		es = "%1 te dio una copia de las llaves de su %2 durante %3 días.";
	};
	class STR_SUCCES_RECEIVE_KEY_PERMA {
		en = "%1 has given you a copy of its %2 keys forever.";
		fr = "%1 vous a donné un double des clés de son/sa %2 pour toujours.";
		de = "%1 hat Ihnen für immer eine Kopie seiner %2-Schlüssel gegeben.";
		es = "%1 te ha dado una copia de sus %2 llaves para siempre.";
	};
	class STR_CONFIRM_HEADER {
		en = "Key exchange";
		fr = "Echange de clés";
		de = "Schlüsselaustausch";
		es = "Intercambio de claves";
	};
	class STR_CONFIRM_GIVE_TEXT {
		en = "%1 wants to give you his %2 in exchange for %3$. Do you accept ?";
		fr = "%1 souhaite vous donner son/sa %2 en échange de %3€. Acceptez-vous ?";
		de = "%1 möchte dir sein %2 im Tausch zu %3€ geben. Akzeptierst du ?";
		es = "%1 quiere darte su %2 por %3€. Aceptas ?";
	};
	class STR_CONFIRM_TEMP_KEYS_TEXT {
		en = "%1 wishes to give you a copy of the keys of his %2 in exchange for %3$ for %4 days. Do you accept ?";
		fr = "%1 souhaite vous donner un double des clés son/sa %2 en échange de %3€ pendant %4 jours. Acceptez-vous ?";
		de = "%1 möchte Ihnen eine Kopie der Schlüssel seines %2 im Austausch für %3€ für %4 Tage geben. Akzeptieren Sie ?";
		es = "%1 desea darle una copia de las llaves de su %2 a cambio de %3 euros por %4 días. ¿ Aceptas ?";
	};
	class STR_CONFIRM_PERMA_KEYS_TEXT {
		en = "%1 wishes to give you a copy of the keys of his %2 in exchange for %3€ forever. Do you accept ?";
		fr = "%1 souhaite vous donner un double des clés son/sa %2 en échange de %3€ pour toujours. Acceptez-vous ?";
		de = "%1 möchte Ihnen eine Kopie der Schlüssel seines %2 im Austausch für %3€ für immer geben. Akzeptieren Sie ?";
		es = "%1 desea darte una copia de las llaves de su %2 a cambio de %3 euros para siempre. ¿ Aceptas ?";
	};
	class STR_REFUSE_GIVE_OBJECT {
		en = "%1 did not wish to exchange keys.";
		fr = "%1 n'as pas souhaité faire l'échange des clés.";
		de = "%1 wollte keine Schlüssel austauschen.";
		es = "%1 no deseaba intercambiar claves.";
	};
	class STR_GIVE_NOT_ENOUGHT_BANK {
		en = "You received an offer from %1 to give you his %2 in exchange for %3$ but you do not have enough money in your bank account. The request was automatically declined.";
		fr = "Vous avez reçu une proposition de %1 pour vous donner son/sa %2 en échange de %3€ mais vous n'avez pas suffisamment d'argent sur votre compte bancaire. La demande a été automatiquement refusée.";
		de = "Sie haben ein Angebot von %1 erhalten, Ihnen seine %2 im Austausch für %3€ zu geben, aber Sie haben nicht genug Geld auf Ihrem Bankkonto. Die Anfrage wurde automatisch abgelehnt.";
		es = "Has recibido una oferta de %1 para darte su %2 a cambio de %3€ pero no tienes suficiente dinero en tu cuenta bancaria. La solicitud fue rechazada automáticamente.";
	};
	class STR_TEMP_PERMA_NOT_ENOUGHT_BANK {
		en = "You received an offer from %1 to give you a copy of the keys to his %2 in exchange for %3$ but you don't have enough money in your bank account. The request was automatically declined.";
		fr = "Vous avez reçu une proposition de %1 pour vous donner un double des clés de son/sa %2 en échange de %3€ mais vous n'avez pas suffisamment d'argent sur votre compte bancaire. La demande a été automatiquement refusée.";
		de = "Sie haben ein Angebot von %1 erhalten, Ihnen eine Kopie der Schlüssel zu seinem %2 im Tausch gegen %3€ zu geben, aber Sie haben nicht genug Geld auf Ihrem Bankkonto. Die Anfrage wurde automatisch abgelehnt.";
		es = "Has recibido una oferta de %1 para darte una copia de las llaves de su %2 a cambio de %3€ pero no tienes suficiente dinero en tu cuenta bancaria. La solicitud fue rechazada automáticamente.";
	};
	class STR_TEMP_NOT_ENOUGHT_BANK {
		en = "You received an offer from %1 to give you a copy of the keys to his %2 in exchange for %3$ for %4 days but you do not have enough money in your bank account. The request was automatically declined.";
		fr = "Vous avez reçu une proposition de %1 pour vous donner un double des clés de son/sa %2 en échange de %3€ pendant %4 jours mais vous n'avez pas suffisamment d'argent sur votre compte bancaire. La demande a été automatiquement refusée.";
		de = "Sie haben ein Angebot von %1 erhalten, Ihnen eine Kopie der Schlüssel zu seinem %2 im Austausch für %3€ für %4 Tage zu geben, aber Sie haben nicht genug Geld auf Ihrem Bankkonto. Die Anfrage wurde automatisch abgelehnt.";
		es = "Has recibido una oferta de %1 para darte una copia de las llaves de su %2 a cambio de %3€ durante %4 días pero no tienes suficiente dinero en tu cuenta bancaria. La solicitud fue rechazada automáticamente.";
	};
	class STR_SEND_CONFIRM {
		en = "Sending a confirmation of the transfer to %1.";
		fr = "Envoie d'une confirmation du transfert à %1.";
		de = "Überweisungsbestätigung wird gesendet an %1.";
		es = "Enviando confirmacion de traspaso a %1.";
	};
	class STR_BUTTON_GIVE_OBJECT {
		en = "Transfer of ownership";
		fr = "Transfert de propriété";
		de = "Übertragung von Eigentum";
		es = "Transferencia de la propiedad";
	};
	class STR_PRICE_DEFAUT_TEXT {
		en = "Price";
		fr = "Prix";
		de = "Preis";
		es = "Precio";
	};
	class STR_HEADER {
		en = "Manage your properties - Warning, if you give your vehicle away, it is permanent !";
		fr = "Gestion de vos propriétés - Attention, si vous donnez votre véhicule, c'est définitif !";
		de = "Verwalten Sie Ihre Immobilien - Achtung, wenn Sie Ihr Fahrzeug verschenken, ist es dauerhaft !";
		es = "Gestiona tus propiedades - Atención, si regalas tu vehículo, ¡ es permanente !";
	};
	class STR_ADDACTION_MENU {
		en = "Manage the keys of your properties";
		fr = "Gestion des clés de vos propriétés";
		de = "Verwalten Sie die Schlüssel Ihrer Eigenschaften";
		es = "Gestionar las claves de sus propiedades";
	};
	class STR_LIST_VEHICLES {
		en = "--- Vehicles ---";
		fr = "--- Véhicules ---";
		de = "--- Fahrzeuge ---";
		es = "--- Vehículos ---";
	};
	class STR_LIST_HOUSES {
		en = "--- Houses ---";
		fr = "--- Maisons ---";
		de = "--- Häuser ---";
		es = "--- Casas ---";
	};
	class STR_NOTHING_IN_LIST {
		en = "You have no vehicle or house belonging to you";
		fr = "Vous n'avez aucun véhicule ou maison vous appartenant";
		de = "Sie haben kein Fahrzeug oder Haus, das Ihnen gehört";
		es = "No tiene ningún vehículo o casa que le pertenezca";
	};
	class STR_OBJECT_LIST {
		en = "%1 %3 - [Distance: %2m]";
		fr = "%1 %3 - [Distance: %2m]";
		de = "%1 %3 - [Entfernung: %2m]";
		es = "%1 %3 - [Distancia: %2m]";
	};
	class STR_DIDNOT_SELECT_OBJECT {
		en = "You must select an object that you definitely want to give !";
		fr = "Vous devez sélectionner un objet que vous voulez définitivement donner !";
		de = "Du musst ein Objekt auswählen, das du definitiv geben willst !";
		es = "Debes seleccionar un objeto que definitivamente quieras regalar !";
	};
	class STR_DIDNOT_SELECT_TEMP_OBJECT {
		en = "You must select an object for which you want to give a copy of the keys !";
		fr = "Vous devez sélectionner un objet dont vous souhaitez donner un double des clés !";
		de = "Sie müssen ein Objekt auswählen, für das Sie eine Kopie der Schlüssel vergeben möchten !";
		es = "Debes seleccionar un objeto al que quiera dar una copia de las claves !";
	};
	class STR_DURATION_DEFAUT_TEXT {
		en = "Duration (days)";
		fr = "Durée (jours)";
		de = "Dauer (tage)";
		es = "Duración (días)";
	};
	class STR_BUTTON_GIVE_KEYS {
		en = "Give a copy of the keys";
		fr = "Donner un double des clés";
		de = "Geben Sie eine Kopie der Tasten";
		es = "Entregar una copia de las llaves";
	};
	class STR_ASK_SEND_OBJECT_TEXT {
		en = "Are you sure you want to give your %1 to %2 for %3$ permanently ? You will never have the keys of this property again.";
		fr = "Êtes-vous certain de vouloir donner votre %1 à %2 pour %3€ définitivement ? Vous n'aurez plus jamais les clés de ce bien.";
		de = "Sind Sie sicher, dass Sie Ihr %1 für %3€ dauerhaft an %2 abgeben wollen ? Sie werden nie wieder die Schlüssel dieser Immobilie haben.";
		es = "¿ Estás seguro de que quieres dar tu %1 a %2 por %3€ de forma permanente ? Nunca más tendrás las llaves de esta propiedad.";
	};
	class STR_ASK_SEND_TEMP_KEY_TEXT {
		en = "Are you sure you want to give a copy of the keys of your %1 to %2 for %3€ for %4 days ?";
		fr = "Êtes vous certain de vouloir donner un double des clés de votre %1 à %2 pour %3€ pendant %4 jours ?";
		de = "Sind Sie sicher, dass Sie eine Kopie der Schlüssel Ihrer %1 bis %2 für %3€ für %4 Tage abgeben wollen ?";
		es = "¿ Estás seguro de que quieres dar una copia de las llaves de tu %1 a %2 por %3 euros durante %4 días ?";
	};
	class STR_ASK_SEND_PERMA_KEY_TEXT {
		en = "Are you sure you want to give a copy of the keys of your %1 to %2 for %3$ forever ?";
		fr = "Êtes vous certain de vouloir donner un double des clés de votre %1 à %2 pour %3€ pour toujours ?";
		de = "Sind Sie sicher, dass Sie Ihrem %1 bis %2 für %3€ für immer eine Kopie der Schlüssel geben wollen ?";
		es = "¿ Estás seguro de que quieres darle una copia de las llaves a tu %1 a %2 por %3 euros para siempre ?";
	};
	class STR_REFUSE_TEMPORARY_OBJECT {
		en = "%1 did not wish to have the copy of the keys.";
		fr = "%1 n'as pas souhaité avoir le double des clés.";
		de = "%1 wollte die Kopie der Schlüssel nicht haben.";
		es = "%1 no quería tener la copia de las llaves.";
	};
	class STR_OWNER_NAME {
		en = "Owner's name";
		fr = "Nom du propriétaire";
		de = "Name des Eigentümers";
		es = "Nombre del propietario";
	};
	class STR_START_DATE {
		en = "You've had the keys since";
		fr = "Vous avez les clés depuis le";
		de = "Sie haben die Schlüssel seit";
		es = "Has tenido las llaves desde";
	};
	class STR_DURATION {
		en = "You have the keys for %1 days";
		fr = "Vous avez les clés pendant %1 jours";
		de = "Sie haben die Schlüssel für %1 Tage";
		es = "Tienes las llaves por %1 días";
	};
	class STR_HEADER_MENU {
		en = "Keys at your disposal";
		fr = "Clés dont vous disposez";
		de = "Schlüssel zu Ihrer Verfügung";
		es = "Llaves a su disposición";
	};
	class STR_NO_VEHICLE {
		en = "No vehicles available";
		fr = "Aucun véhicule disponible";
		de = "Keine Fahrzeuge verfügbar";
		es = "No hay vehículos disponibles";
	};
	class STR_VEHICLE_LIST {
		en = "List of vehicles";
		fr = "Liste des véhicules";
		de = "Liste der Fahrzeuge";
		es = "Lista de vehículos";
	};
    class STR_KEY_UNIMPOUND_FROM {
		en = "Garage for rented vehicles";
        fr = "Garage des véhicules en prêts";
		de = "Garage für Mietfahrzeuge";
		es = "Garaje para vehículos de alquiler";
    };
    class STR_KEY_UNIMPOUND_DESCRIPTION {
		en = "Unimpound the vehicle : %1 belonging to %2";
        fr = "Sortie du véhicule : %1 appartenant à %2";
		de = "Beschlagnahme des Fahrzeugs : %1 gehört zu %2";
		es = "Liberar el vehículo : %1 perteneciente a %2";
    };
    class STR_CANNOT_OPEN_GARAGE {
		en = "Unfortunately you can't take out vehicles with duplicate keys. You have to wait for the owner to log on.";
        fr = "Malheuresement vous ne pouvez pas sortir les véhicules dont vous avez un double des clés. Vous devez attendre que le propriétaire se connecte.";
		de = "Leider kann man Fahrzeuge mit Nachschlüsseln nicht herausnehmen. Sie müssen darauf warten, dass sich der Besitzer einloggt.";
		es = "Desafortunadamente no se pueden sacar vehículos con llaves duplicadas. Tienes que esperar a que el propietario se conecte.";
    };
    class STR_CANNOT_GIVE_PERMANENTLY {
		en = "You can't permanently give away a copy of your keys !";
        fr = "Vous ne pouvez pas donner de manière permanante un double de vos clés !";
		de = "Sie können nicht dauerhaft eine Kopie Ihrer Schlüssel verschenken !";
		es = "¡ No puedes regalar permanentemente una copia de tus llaves !";
    };
    class STR_CANNOT_GIVE_KEY_TEMPORARY {
		en = "You can't temporarily give away a copy of your keys !";
        fr = "Vous ne pouvez pas donner temporairement un double de vos clés !";
		de = "Sie können nicht vorübergehend eine Kopie Ihrer Schlüssel verschenken !";
		es = "¡ No puedes regalar temporalmente una copia de tus llaves !";
    };
    class STR_MIN_TEMP_KEY {
		en = "You must give a copy of your keys for %1 days minimum !";
        fr = "Vous devez donner un double de vos clés pendant %1 jours minimum !";
		de = "Sie müssen eine Kopie Ihrer Schlüssel für mindestens %1 Tag abgeben !";
		es = "¡ Debe dar una copia de sus llaves por un mínimo de %1 días !";
    };
    class STR_MAX_TEMP_KEY {
		en = "You can't give away a copy of your keys for %1 days maximum !";
        fr = "Vous ne pouvez donner un double de vos clés pendant %1 jours maximum !";
		de = "Sie können eine Kopie Ihrer Schlüssel für maximal %1 Tag nicht verschenken !";
		es = "¡ No puedes regalar una copia de tus llaves por un máximo de %1 días !";
    };
	class STR_SUCCES_KEY_TEMP_OBJECT {
		en = "You gave a copy of the keys of your %1 to %2 for %3 days.";
		fr = "Vous avez donné un double des clés de votre %1 à %2 pendant %3 jours.";
		de = "Sie gaben eine Kopie der Schlüssel Ihrer %1 bis %2 für %3 Tagen.";
		es = "Le diste una copia de las llaves de tu %1 a %2 por %3 días.";
	};
	class STR_SUCCES_KEY_PERMA_OBJECT {
		en = "You have given a copy of the keys to your %1 to %2 forever.";
		fr = "Vous avez donné un double des clés de votre %1 à %2 pour toujours.";
		de = "Sie haben Ihren %1 bis %2 für immer eine Kopie der Schlüssel gegeben.";
		es = "Le has dado una copia de las llaves a tus %1 a %2 para siempre.";
	};
	class STR_ALREADY_HAVE_KEYS {
		en = "The player already has a copy of the keys of this property.";
		fr = "Le joueur a déjà un double des clés de ce bien.";
		de = "Der Spieler verfügt bereits über eine Kopie der Schlüssel dieser Eigenschaft.";
		es = "El jugador ya tiene una copia de las llaves de esta propiedad.";
	};
	class STR_CANNOT_SELL_HOUSE {
		en = "You can't resell a house that doesn't belong to you !";
		fr = "Vous ne pouvez pas revendre une maison qui ne vous appartient pas !";
		de = "Ein Haus, das Ihnen nicht gehört, können Sie nicht weiterverkaufen !";
		es = "¡ No puedes revender una casa que no te pertenece !";
	};
	class STR_PUT_A_PRICE {
		en = "Please put a price that the receiver will have to pay to accept ! You can put 0.";
		fr = "Veuillez mettre un prix que le destinataire devra payer pour accepter ! Vous pouvez mettre 0.";
		de = "Bitte geben Sie einen Preis an, den der Empfänger zahlen muss, um es anzunehmen ! Sie können 0 eingeben.";
		es = "¡ Por favor, ponga un precio que el receptor tendrá que pagar para aceptar ! Puede poner 0.";
	};
	class STR_PUT_A_DURATION {
		en = "Please enter a loan period ! You can enter 0 to give a copy of the keys forever.";
		fr = "Veuillez mettre une durée de prêt ! Vous pouvez mettre 0 pour donner un double des clés pour toujours.";
		de = "Bitte geben Sie eine Leihfrist ein ! Sie können 0 eingeben, um eine Kopie der Schlüssel für immer zu geben.";
		es = "¡ Por favor, introduzca un período de préstamo ! Puede poner 0 para dar una copia de las llaves para siempre.";
	};
	class STR_GARAGE_INFOS_LICENSE_PLATE {
		en = "License plate :";
		fr = "Plaque d'immatriculation :";
		de = "Kfz-Kennzeichen :";
		es = "Placa de matrícula :";
	};
	class STR_GARAGE_INFOS_TECHNICAL_INSPECTION {
		en = "Technical inspection status :";
		fr = "État du contrôle technique :";
		de = "Stand der technischen Überwachung :";
		es = "Estado de la inspección técnica :";
	};
	class STR_GARAGE_INFOS_FUEL_TYPE {
		en = "Type of fuel :";
		fr = "Type d'essence :";
		de = "Kraftstoffart :";
		es = "Tipo de combustible :";
	};
	class STR_HOUSE_REPLACED_CHAR_BY {
		en = "and";
		fr = "et";
		de = "und";
		es = "y";
	};
};