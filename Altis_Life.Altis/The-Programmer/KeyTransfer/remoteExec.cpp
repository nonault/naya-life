class max_keytransfer_fnc_giveVehicle {
    allowedTargets = 2;
};
class max_keytransfer_fnc_giveHouse {
    allowedTargets = 2;
};
class max_keytransfer_fnc_ownerDisplayResultGive {
    allowedTargets = 1;
};
class max_keytransfer_fnc_receiveConfirmationGive {
    allowedTargets = 1;
};
class max_keytransfer_fnc_initTemporaryKeys {
    allowedTargets = 2;
};
class max_keytransfer_fnc_initTemporaryKeysVariables {
    allowedTargets = 1;
};
class max_keytransfer_fnc_addTemporaryKeys {
    allowedTargets = 2;
};
class max_keytransfer_fnc_receiveConfirmationKeys {
    allowedTargets = 1;
};
class max_keytransfer_fnc_ownerDisplayResultKeys {
    allowedTargets = 1;
};
class max_keytransfer_fnc_getTemporaryVehicles {
    allowedTargets = 2;
};
class max_keytransfer_fnc_temporaryKeysMenu {
    allowedTargets = 1;
};
class max_keytransfer_fnc_fetchGivenKeys {
    allowedTargets = 2;
};
class max_keytransfer_fnc_giveVehicleKey {
    allowedTargets = 1;
};
class max_keytransfer_fnc_deleteHouseMarker {
    allowedTargets = 1;
};
class max_keytransfer_fnc_removeTemporaryKeys {
    allowedTargets = 2;
};