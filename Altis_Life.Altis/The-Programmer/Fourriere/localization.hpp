class Fourriere_Localization {
	class STR_HEADER_MENU {
		en = "Impound Menu";
		fr = "Fourrière";
		de = "Beschlagnahmungs Menü";
		es = "Menú Confiscaciones";
	};
	class STR_UNIMPOUND_FEE {
		en = "Vehicle unimpound tax";
		fr = "Taxe de sortie du véhicule";
		de = "Ausreisesteuer";
		es = "Impuesto de salida de vehículos";
	};
	class STR_OWNER {
		en = "Owner";
		fr = "Propriétaire";
		de = "Besitzer";
		es = "Propietario";
	};
	class STR_MODEL {
		en = "Model";
		fr = "Modèle";
		de = "Automodell";
		es = "Modelo";
	};
	class STR_COLOR {
		en = "Color";
		fr = "Couleur";
		de = "Farbe";
		es = "Color";
	};
	class STR_MONEY {
		en = "$";
		fr = "€";
		de = "€";
		es = "€";
	};
	class STR_WAIT {
		en = "Loading the impound...";
		fr = "Chargement de la fourrière...";
		de = "Laden des Menüs der Landzunge...";
		es = "Cargando el menú de la cabecera del coche...";
	};
	class STR_CONFIRM_UNIMPOUND_HEADER {
		en = "Exiting a vehicle not owned by you";
		fr = "Sortie d'un véhicle dont vous n'êtes pas le propriétaire";
		de = "Verlassen eines nicht Ihnen gehörenden Fahrzeugs";
		es = "Salir de un vehículo que no es de su propiedad";
	};
	class STR_CONFIRM_UNIMPOUND_TEXT {
		en = "You are about to take the %1 vehicle out of the garage, you will get the keys to that vehicle and you have an obligation to return it to its owner.";
		fr = "Vous êtes sur le point de sortir du garage le véhicule de %1, vous obtiendrez les clés de ce véhicule et vous avez l'obligation de le rendre à son propriétaire.";
		de = "Sie sind im Begriff, das %1 Fahrzeug aus der Garage zu nehmen, Sie erhalten die Schlüssel zu diesem Fahrzeug und Sie haben die Verpflichtung, es an seinen Besitzer zurückzugeben.";
		es = "Usted está a punto de sacar el vehículo %1 del garaje, recibirá las llaves de ese vehículo y tiene la obligación de devolverlo a su propietario.";
	};
	class STR_CONFIRM {
		en = "Continue";
		fr = "Continuer";
		de = "Weitermachen";
		es = "Continuar";
	};
	class STR_CANCEL {
		en = "Cancel";
		fr = "Annuler";
		de = "Annullierer";
		es = "Cancelar";
	};
	class STR_NO_VEHICLE {
		en = "No vehicles available";
		fr = "Aucun véhicule disponible";
		de = "Keine Fahrzeuge verfügbar";
		es = "No hay vehículos disponibles";
	};
	class STR_VEHICLE_LIST {
		en = "List of vehicles";
		fr = "Liste des véhicules";
		de = "Liste der Fahrzeuge";
		es = "Lista de vehículos";
	};
	class STR_MASTERPAY_TITLE {
		en = "Government";
		fr = "Gouvernement";
		de = "Regierung";
		es = "Gobierno";
	};
	class STR_MASTERPAY_TEXT {
		en = "Impoundment of : %1";
		fr = "Mise en fourrière de : %1";
		de = "Auferlegung von : %1";
		es = "Confiscación de : %1";
	};
	class STR_BONUS_TO_COP_ACCOUNT {
		en = "You impounded a %1\n\n %2$ were donated to the police account for cleaning up the streets !";
		fr = "Vous avez mis en fourrière un(e) %1\n\n %2€ ont été donnés au compte de la police pour avoir nettoyé les rues !";
		de = "Du hast einen %1\n\n %2€ beschlagnahmt, der dem Polizeikonto für die Reinigung der Straßen gespendet wurde !";
		es = "Confiscaste un %1\n\n %2€ que fueron donados a la cuenta de la policía para limpiar las calles !";
	};
	class STR_NOT_ALLOWED {
		en = "You're not allowed in the impound warehouse !";
		fr = "Vous n'êtes pas autorisé à accéder aux entrepôts de la fourrière !";
		de = "Sie dürfen nicht in das beschlagnahmte Lagerhaus !";
		es = "¡ No se le permite entrar en el almacén de incautaciones !";
	};
	class STR_SEARCH_TITLE {
		en = "Search by : Owner's name";
		fr = "Rechercher par : Nom du propriétaire";
		de = "Suche nach : Name des Eigentümers";
		es = "Buscar por : Nombre del propietario";
	};
	class STR_SEARCH {
		en = "Search";
		fr = "Chercher";
		de = "Suche";
		es = "Busca";
	};
	class STR_IMPOUND_VEHICLE_NO_BONUS {
		en = "You impounded a %1 !";
		fr = "Vous avez mis en fourrière un(e) %1 !";
		de = "Sie haben einen %1 beschlagnahmt !";
		es = "¡ Has confiscado un %1 !";
	};
};