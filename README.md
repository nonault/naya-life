# Naya Life - Projet serveur arma 3

## Informations

Le dossier life_server dois être transformé en format .pbo qui sera ajouté dans le dossier addons du @life_server

le dossier addons présent est à ajouter dans @The_Programmer à la racine du serveur.

## Requêtes SQL : 

License plates : ALTER TABLE `vehicles` ADD COLUMN `immatriculation` TEXT NOT NULL;

Impound menu (fourriere) : ALTER TABLE `vehicles` ADD COLUMN `fourriere` INT(1) NOT NULL DEFAULT '0';

Key transfer : CREATE TABLE `temporary_keys` (
`id` INT(100) NOT NULL AUTO_INCREMENT,
`type` TEXT NOT NULL,
`object_id` INT(6) NOT NULL,
`object_details` TEXT NOT NULL,
`pid` VARCHAR(17) NOT NULL,
`insert_time` TEXT NOT NULL,
`delete_after` INT(6) NOT NULL DEFAULT '1',
`delete_date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

## Lignes de code des panneaux :

License plates : 

    civil : _null=this execVM "The-Programmer\Immatriculation\init.sqf";
    gendarmerie : _null=this execVM "The-Programmer\Immatriculation\initCop.sqf";

Fourriere : 

    Terrestre : this addAction["Fourriere Terrestre",{["Car","spawn_veh"] spawn max_fourriere_fnc_openFourriere;},"",0,false,false,"([] call max_fourriere_fnc_checkConditionFourriere)"];
    Aerienne : this addAction["Fourriere Aerienne",{["Air","spawn_veh"] spawn max_fourriere_fnc_openFourriere;},"",0,false,false,"([] call max_fourriere_fnc_checkConditionFourriere)"];
    Maritime : this addAction["Fourriere Maritime",{["Ship","spawn_veh"] spawn max_fourriere_fnc_openFourriere;},"",0,false,false,"([] call max_fourriere_fnc_checkConditionFourriere)"];

    Pour faire spawn les vehicules devant le pnj, il faut mettre un marqueur vide sur la map et lui donner un nom de variable. 
    Cette variable remplacera "spawn_veh" dans le init du panneau/pnj

Key Transfer :

    transfer de clés : _null=this execVM "The-Programmer\KeyTransfer\init.sqf";
    garage de prêt : this addAction ["Garage des véhicules en prêts",{["Car","spawn_veh"] spawn max_keytransfer_fnc_openTemporaryKeysMenu;}];
    
    Pour faire spawn les vehicules devant le pnj, il faut mettre un marqueur vide sur la map et lui donner un nom de variable. 
    Cette variable remplacera "spawn_veh" dans le init du panneau/pnj






